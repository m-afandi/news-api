let container = document.getElementById('card');

function setLink(keyword){
  let link = `https://newsapi.org/v2/everything?q=${keyword}&apiKey=24dbd2dd813747fe8e80328c4a608f9f`;
  return fetch(link);
}

document.getElementById('search').addEventListener('keydown', function() {
  setTimeout(() => (this.value.length > 0)? setFetch(this.value): setFetch('Indonesia'), 250);
});

function setFetch(key){
  setLink(key)
    .finally(container.innerHTML = '<h5 class="text-center mt-5">Please Wait...</h5>')
    .then(response => response.json())
    .then(data => {
      let article = data['articles'];
      let str = "";

      if(article.length > 0){
        article.forEach(index => {
          let title = (index['title'].length > 90)? index['title'].slice(0, 90) : index['title'];
          let author = (index['author'] != null)? ((index['author'].length > 55)? index['author'].slice(0, 55): index['author']) : 'Unknown';
          let desc = index['description'].replace(/[<>/"]/gi, "");

          str += `<div class="col-12 col-md-6 col-lg-4">
            <div class="card mb-4 shadow-sm">
              <img class="card-img-top" height=200px src="${index['urlToImage']}" alt="Thumnail">
              <div class="card-body">
                <h5 class="card-title" style="height:70px">${title}</h5>
                <h6 class="card-subtitle mb-2 text-muted" style="height:40px">${author} - ${index['publishedAt']}</h6>
                <p class="card-text" style="height: 80px; overflow-y: scroll;">${desc}</p>
                <a href="${index['url']}" class="btn btn-primary">Read More...</a>
              </div>
            </div>
          </div>`;
        });
        container.innerHTML = str;
      }else{
        str = `<div class='alert alert-danger' id='alert' role='alert'>
                Maaf, content tidak ditemukan 
              </div>`;
        container.innerHTML = str;
      }
    })
  .catch((error) => {
    str = `<div class='alert alert-danger' id='alert' role='alert'>
            Maaf, sedang terjadi kesalahan
          </div>`;
    container.innerHTML = str;
    console.log(error);
  })
}

setFetch('Indonesia');